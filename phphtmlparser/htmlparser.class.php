<?php

/*
 * Copyright (c) 2003 Jose Solorzano.  All rights reserved.
 * Redistribution of source must retain this copyright notice.
 *
 * Jose Solorzano (http://jexpert.us) is a software consultant.
 *
 * Contributions by:
 * - Leo West (performance improvements)
 */

define ("NODE_TYPE_START", 0);
define ("NODE_TYPE_ELEMENT", 1);
define ("NODE_TYPE_ENDELEMENT", 2);
define ("NODE_TYPE_TEXT", 3);
define ("NODE_TYPE_COMMENT", 4);
define ("NODE_TYPE_DONE", 5);

/**
 * Class html_parser.
 * To use, create an instance of the class passing
 * HTML text. Then invoke parse() until it's false.
 * When parse() returns true, $i_node_type, $i_node_name
 * $i_node_value and $i_node_attributes are updated.
 *
 * To create an html_parser instance you may also
 * use convenience functions html_parser_ForFile
 * and html_parser_ForURL.
 */
class html_parser {

    /**
     * Field i_node_type.
     * May be one of the NODE_TYPE_* constants above.
     */
    var $i_node_type;

    /**
     * Field i_node_name.
     * For elements, it's the name of the element.
     */
    var $i_node_name = "";

    /**
     * Field i_node_value.
     * For text nodes, it's the text.
     */
    var $i_node_value = "";

    /**
     * Field i_node_attributes.
     * A string-indexed array containing attribute values
     * of the current node. Indexes are always lowercase.
     */
    var $i_node_attributes;

    // The following fields should be
    // considered private:

    var $i_html_text;
    var $i_html_text_length;
    var $i_html_text_index = 0;
    var $i_html_current_char;
    var $BOE_ARRAY;
    var $B_ARRAY;
    var $BOS_ARRAY;

    /**
     * Constructor.
     * Constructs an html_parser instance with
     * the HTML text given.
     */
    function html_parser($a_html_text) {
        $this->i_html_text = $a_html_text;
        $this->i_html_text_length = strlen($a_html_text);
        $this->i_node_attributes = array();
        $this->set_text_index(0);

        $this->BOE_ARRAY = array(" ", "\t", "\r", "\n", "=" );
        $this->B_ARRAY = array(" ", "\t", "\r", "\n" );
        $this->BOS_ARRAY = array(" ", "\t", "\r", "\n", "/" );
    }

    /**
     * Method parse.
     * Parses the next node. Returns false only if
     * the end of the HTML text has been reached.
     * Updates values of iNode* fields.
     */
    function parse() {
        $text = $this->skip_to_element();
        if ($text != "") {
            $this->i_node_type = NODE_TYPE_TEXT;
            $this->i_node_name = "Text";
            $this->i_node_value = $text;
            return true;
        }
        return $this->read_tag();
    }

    function clear_attributes() {
        $this->i_node_attributes = array();
    }

    function read_tag() {
        if ($this->i_current_char != "<") {
            $this->i_node_type = NODE_TYPE_DONE;
            return false;
        }
        $this->clear_attributes();
        $this->skip_max_in_tag("<", 1);
        if ($this->i_current_char == '/') {
            $this->move_next();
            $name = $this->skip_to_blanks_in_tag();
            $this->i_node_type = NODE_TYPE_ENDELEMENT;
            $this->i_node_name = $name;
            $this->i_node_value = "";
            $this->skip_end_of_tag();
            return true;
        }
        $name = $this->skip_to_blanks_or_slash_in_tag();
        if (!$this->is_valid_tag_identifier($name)) {
                $comment = false;
                if (strpos($name, "!--") === 0) {
                    $ppos = strpos($name, "--", 3);
                    if (strpos($name, "--", 3) === (strlen($name) - 2)) {
                        $this->i_node_type = NODE_TYPE_COMMENT;
                        $this->i_node_name = "Comment";
                        $this->i_node_value = "<". $name .">";
                        $comment = true;
                    }
                    else {
                        $rest = $this->skip_to_string_in_tag ("-->");
                        if ($rest != "") {
                            $this->i_node_type = NODE_TYPE_COMMENT;
                            $this->i_node_name = "Comment";
                            $this->i_node_value = "<" . $name . $rest;
                            $comment = true;
                            // Already skipped end of tag
                            return true;
                        }
                    }
                }
                if (!$comment) {
                    $this->i_node_type = NODE_TYPE_TEXT;
                    $this->i_node_name = "Text";
                    $this->i_node_value = "<" . $name;
                    return true;
                }
        }
        else {
                $this->i_node_type = NODE_TYPE_ELEMENT;
                $this->i_node_value = "";
                $this->i_node_name = $name;
                while ($this->skip_blanks_in_tag()) {
                    $attr_name = $this->skip_to_blanks_or_equals_in_tag();
                    if ($attr_name != "" && $attr_name != "/") {
                        $this->skip_blanks_in_tag();
                        if ($this->i_current_char == "=") {
                            $this->skip_equals_in_tag();
                            $this->skip_blanks_in_tag();
                            $value = $this->read_value_in_tag();
                            $this->i_node_attributes[strtolower($attr_name)] = $value;
                        }
                        else {
                            $this->i_node_attributes[strtolower($attr_name)] = "";
                        }
                    }
                }
        }
        $this->skip_end_of_tag();
        return true;
    }

    function is_valid_tag_identifier($name) {
        return ereg("^[A-Za-z0-9_\\-]+$", $name);
    }

    function skip_blanks_in_tag() {
        return "" != ($this->skip_in_tag($this->B_ARRAY));
    }

    function skip_to_blanks_or_equals_in_tag() {
        return $this->skip_to_in_tag($this->BOE_ARRAY);
    }

    function skip_to_blanks_in_tag() {
        return $this->skip_to_in_tag($this->B_ARRAY);
    }

    function skip_to_blanks_or_slash_in_tag() {
        return $this->skip_to_in_tag($this->BOS_ARRAY);
    }

    function skip_equals_in_tag() {
        return $this->skip_max_in_tag("=", 1);
    }

    function read_value_in_tag() {
        $ch = $this->i_current_char;
        $value = "";
        if ($ch == "\"") {
            $this->skip_max_in_tag("\"", 1);
            $value = $this->skip_to_in_tag("\"");
            $this->skip_max_in_tag("\"", 1);
        }
        else if ($ch == "'") {
            $this->skip_max_in_tag("'", 1);
            $value = $this->skip_to_in_tag("'");
            $this->skip_max_in_tag("'", 1);
        }
        else {
            $value = $this->skip_to_blanks_in_tag();
        }
        return $value;
    }

    function set_text_index($index) {
        $this->i_html_text_index = $index;
        if ($index >= $this->i_html_text_length) {
            $this->i_current_char = -1;
        }
        else {
            $this->i_current_char = $this->i_html_text{$index};
        }
    }

    function move_next() {
        if ($this->i_html_text_index < $this->i_html_text_length) {
            $this->set_text_index($this->i_html_text_index + 1);
            return true;
        }
        else {
            return false;
        }
    }

    function skip_end_of_tag() {
        while (($ch = $this->i_current_char) !== -1) {
            if ($ch == ">") {
                $this->move_next();
                return;
            }
            $this->move_next();
        }
    }

    function skip_in_tag($chars) {
        $sb = "";
        while (($ch = $this->i_current_char) !== -1) {
            if ($ch == ">") {
                return $sb;
            }
            else {
                $match = false;
                for ($idx = 0; $idx < count($chars); $idx++) {
                    if ($ch == $chars[$idx]) {
                        $match = true;
                        break;
                    }
                }
                if (!$match) {
                    return $sb;
                }
                $sb .= $ch;
                $this->move_next();
            }
        }
        return $sb;
    }

    function skip_max_in_tag($chars, $max_chars) {
        $sb = "";
        $count = 0;
        while (($ch = $this->i_current_char) !== -1 && $count++ < $max_chars) {
            if ($ch == ">") {
                return $sb;
            }
            else {
                $match = false;
                for ($idx = 0; $idx < count($chars); $idx++) {
                    if ($ch == $chars[$idx]) {
                        $match = true;
                        break;
                    }
                }
                if (!$match) {
                    return $sb;
                }
                $sb .= $ch;
                $this->move_next();
            }
        }
        return $sb;
    }

    function skip_to_in_tag($chars) {
        $sb = "";
        while (($ch = $this->i_current_char) !== -1) {
            $match = $ch == ">";
            if (!$match) {
                for ($idx = 0; $idx < count($chars); $idx++) {
                    if ($ch == $chars[$idx]) {
                        $match = true;
                        break;
                    }
                }
            }
            if ($match) {
                return $sb;
            }
            $sb .= $ch;
            $this->move_next();
        }
        return $sb;
    }

    function skip_to_element() {
        $sb = "";
        while (($ch = $this->i_current_char) !== -1) {
            if ($ch == "<") {
                return $sb;
            }
            $sb .= $ch;
            $this->move_next();
        }
        return $sb;
    }

    /**
     * Returns text between current position and $needle,
     * inclusive, or "" if not found. The current index is moved to a point
     * after the location of $needle, or not moved at all
     * if nothing is found.
     */
    function skip_to_string_in_tag($needle) {
        $pos = strpos($this->i_html_text, $needle, $this->i_html_text_index);
        if ($pos === false) {
            return "";
        }
        $top = $pos + strlen($needle);
        $retvalue = substr($this->i_html_text, $this->i_html_text_index, $top - $this->i_html_text_index);
        $this->set_text_index($top);
        return $retvalue;
    }
}

?>